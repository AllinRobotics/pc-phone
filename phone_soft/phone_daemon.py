import serial   
import os, time, sys, stat 
import RPi.GPIO as GPIO
import mmap
import struct


class Phone():

    def __init__(self):
        self.power = 0
        # Enable Serial Communication
        self.port = serial.Serial("/dev/ttyS0", baudrate=115200, timeout=5)


    def power_on_phone(self):
        #seting up gpio ping
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(12, GPIO.OUT)

        #counter and triger for power up
        count = 3
        
        #define filters for wake up message
        line = ""
        new_line = "\r\n"
        ok_answer = "OK\r\n"


        #here check if power is set to false if it is true try to  power on
        while self.power==0:
            #try to connect "cont" times
            self.port.write('AT'+'\r\n')
            time.sleep(0.5)

            cmgs = self.port.readline()
            
            #filtering message loking for a clear answer
            if (cmgs != line and cmgs != new_line):
                if cmgs == ok_answer:
                    self.power = 1

                #try to wake up sim_module
            if count == 0:
                #turn on pin
                GPIO.output(12, 1)
                time.sleep(3)
                #turn off pin
                GPIO.output(12, 0)
                time.sleep(10)
                count = 20


                
            count -= 1
            print count

    def setting_up(self):
        # Transmitting AT Commands to the Modem
        # '\r\n' indicates the Enter key
        if self.power == 1:
            self.port.write('AT'+'\r\n')
            time.sleep(0.5)

            self.port.write('AT+CLIP=1'+'\r\n')
            time.sleep(0.5)

            self.port.write('AT+CMGF=1'+'\r\n')
            time.sleep(0.5)

            self.port.write('AT+CNMI=2,2,0,0,0'+'\r\n')
            time.sleep(0.5)


    def reading(self):
        try:
            global buf, s
            while True:
                cmgs = self.port.readline()
                new_s = struct.unpack('15s', buf[1:16])

                if  s!=new_s:
                    s = new_s
                    print "%s"% s
                    self.port.write('%s'% s +'\r\n')                    
                    #s.raw = '            '
                #time.sleep(0.2)
                if cmgs!="\r\n":
                    print cmgs
                    #define filters for wake up message

                    line = ""
                    new_line = "\r\n"
                    ok_answer = "OK\r\n"

                    #filtering message loking for a clear answer
                    if (cmgs != line and cmgs != new_line):
                        print cmgs

                        if cmgs == ok_answer:
                            print("Todo ok")


        except KeyboardInterrupt, e:
            logging.info("Stopping...")


if __name__ == '__main__':
    # Open the file for reading
    fd = os.open('/tmp/mmaphone', os.O_CREAT | os.O_TRUNC | os.O_RDWR)

    # Zero out the file to insure it's the right size
    assert os.write(fd, '\x00' * mmap.PAGESIZE) == mmap.PAGESIZE

    # Memory map the file
    buf = mmap.mmap(fd, mmap.PAGESIZE, mmap.MAP_SHARED, mmap.PROT_WRITE)
                              
    os.chmod("/tmp/mmaphone", stat.S_IRWXO|stat.S_IRWXO|stat.S_IRWXO) 

    s = None

    p = Phone()
    p.power_on_phone()
    p.setting_up()
    p.reading()
